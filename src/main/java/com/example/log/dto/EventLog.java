package com.example.log.dto;

import lombok.Data;

@Data
public class EventLog {
    private String userId;
    private String url;
    private String ip;
}
