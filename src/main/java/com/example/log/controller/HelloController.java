package com.example.log.controller;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.event.AnalysisEventListener;
import com.example.log.dto.User;
import com.example.log.service.LoginService;
import com.example.log.utils.ExcelUtils;
import com.example.log.utils.ZipDownloadUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RestController
@RequestMapping("/v1")
public class HelloController {

    @Autowired
    private LoginService service;

    @GetMapping("/hello")
    public String helloWorld(@RequestParam String userId){
        System.out.println(userId);
        return "hello world";
    }

    @GetMapping("/login")
    public String login(@RequestParam String username, @RequestParam String password){
        return "success";
    }

    @PostMapping("/upload")
    public String upload(@RequestParam("file")MultipartFile file) throws Exception{
        if(file.isEmpty()){
            return "文件不能为空";
        }
        try{
            EasyExcel.read(file.getInputStream(),User.class,new ExcelUtils(service))
                    .sheet()
                    .headRowNumber(1)
                    .doRead();
        }catch (Exception e){
            return "导入失败";
        }
        return "导入成功";
    }

    @GetMapping("/download")
    public void download(HttpServletResponse response){

        try {
            ZipDownloadUtils.download("F:/excel","F:/excel.zip",response);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
