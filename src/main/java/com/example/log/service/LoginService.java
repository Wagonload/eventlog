package com.example.log.service;

import com.example.log.dao.LoginDao;
import com.example.log.dto.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.function.Consumer;

@Service
public class LoginService {

    @Autowired
    private LoginDao loginDao;

    public Consumer<List<User>> batchInsert(){
        return data -> login(data);
    }

    public boolean login(List<User> user){
        loginDao.saveAll(user);
        return false;
    }

}
