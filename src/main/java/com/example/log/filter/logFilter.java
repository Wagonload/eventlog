package com.example.log.filter;

import com.example.log.dto.EventLog;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class logFilter extends OncePerRequestFilter{

    private static Logger log = LoggerFactory.getLogger("event_log");

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        String userId = (String) request.getParameter("userId");
        String url = request.getRequestURI();

        EventLog eventLog = new EventLog();
        eventLog.setUrl(url);
        eventLog.setUserId(userId);

        log.info(eventLog.toString());
        filterChain.doFilter(request,response);
    }
}
