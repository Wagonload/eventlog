package com.example.log.utils;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.zip.Adler32;
import java.util.zip.CheckedOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class ZipDownloadUtils {

    public static void download(String filePath,String zipName,HttpServletResponse response) throws IOException {
        File file = new File(filePath);
        File zipFile = new File(zipName);

        if(file.isDirectory()){
            File[] files = file.listFiles();

            FileOutputStream fout = new FileOutputStream(zipFile);
            CheckedOutputStream checksum = new CheckedOutputStream(fout, new Adler32());
            ZipOutputStream zout = new ZipOutputStream(checksum);

            FileInputStream fin = null;

            for(int i = 0;i < files.length;i++){
                fin = new FileInputStream(files[i].getAbsolutePath());

                int length;
                byte[] buffer = new byte[1024];
                while((length = fin.read(buffer)) > 0) {
                    zout.write(buffer, 0, length);
                }

            }

            zout.closeEntry();
            zout.finish();
            fin.close();
            zout.close();

        }
        /*  设置文件ContentType类型，这样设置，会自动判断下载文件类型   */
        response.setContentType("application/multipart/form-data");
    }

}
