package com.example.log.utils;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.example.log.dto.User;
import com.example.log.service.LoginService;

import java.util.ArrayList;
import java.util.List;

public class ExcelUtils extends AnalysisEventListener<User> {

    private List<User> list = new ArrayList<>();

    private LoginService service;

    public ExcelUtils(LoginService service) {
        this.service = service;
    }

    @Override
    public void invoke(User user, AnalysisContext analysisContext) {
        if(!user.getPassword().equals("123")){

            list.add(user);
        }

    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {
        service.login(list);
    }


}
