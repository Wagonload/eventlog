package com.example.log.dao;

import com.example.log.dto.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

@Component
public interface LoginDao extends CrudRepository<User,Long> {

}
